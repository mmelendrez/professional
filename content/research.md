---
title: "Research"
---

1. Create new single page

    ```
    hugo new research.md
    ```
1. Remove the `draft` and `date` field from the top part of the markdown field
