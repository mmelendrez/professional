---
title: "Testpost"
date: 2019-03-24T17:52:38-05:00
---

1. Create new blog post via

    ```
    hugo new post/posttitle.md
    ```
1. Remove `draft` from top part of md file
